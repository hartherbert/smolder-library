export interface IPicture {
  // id: string;
  // path: string;
  url: string;
  // format: 'jpg' | 'png' | 'gif';
  // size: number; // in bytes
  // qualityPoints?: IPictureQualityPoint[];
}

export interface IPictureQualityPoint {
  id?: string;
  userId?: string;
  isHighQuality: boolean; // true => +1; false => -1;
  createdAt?: string;
  updatedAt?: string;
}
