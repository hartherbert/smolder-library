import { IPlaceType } from './place';
import { ILocation } from './location';

export interface ISearch {
  id?: string;
  userId?: string;
  placeType: IPlaceType;

  location: ILocation;

  createdAt?: string;
  updatedAt?: string;
}
