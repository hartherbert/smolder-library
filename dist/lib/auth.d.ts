export interface AuthLoginTicket {
    authToken: string;
    refreshToken?: string;
}
export declare enum LoginMethod {
    Google = "google",
    Facebook = "facebook",
    Twitter = "twitter",
    Custom = "custom"
}
export interface ILoginData {
    method: LoginMethod;
    code: string;
}
