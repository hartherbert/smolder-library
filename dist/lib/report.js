"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IReportType;
(function (IReportType) {
    IReportType["NotAvailableAnymore"] = "not available anymore";
    IReportType["Malfunction"] = "malfunction";
    IReportType["TemporarilyClosed"] = "temporarily closed";
    IReportType["Custom"] = "custom";
})(IReportType = exports.IReportType || (exports.IReportType = {}));
//# sourceMappingURL=report.js.map