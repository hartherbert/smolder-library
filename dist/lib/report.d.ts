export declare enum IReportType {
    NotAvailableAnymore = "not available anymore",
    Malfunction = "malfunction",
    TemporarilyClosed = "temporarily closed",
    Custom = "custom"
}
export interface IReport {
    id?: string;
    type: IReportType;
    placeId?: string;
    userId?: string;
    body?: string;
    checked?: boolean;
    active?: boolean;
    createdAt?: string;
    updatedAt?: string;
}
