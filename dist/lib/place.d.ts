import { IPicture } from './picture';
import { ILocation } from './location';
export declare enum IPlaceType {
    ATM = "atm",
    Tobacco = "tobacco",
    Cigarettes = "cigarettes",
    Fuel = "fuel",
    Bar = "bar",
    Pub = "pub",
    Bank = "bank",
    Pharmacy = "pharmacy",
    Casino = "casino",
    NightClub = "nightclub",
    Toilets = "toilets",
    Hospital = "hospital",
    Alcohol = "alcohol",
    Kiosk = "kiosk"
}
export declare enum IPlaceState {
    New = "new",
    Verified = "verified",
    Perchance = "perchance",
    Outdated = "outdated"
}
export declare enum IPlaceDetailType {
    Name = "name",
    OpeningHours = "opening",
    IsInside = "inside",
    Brand = "brand",
    Email = "email",
    Website = "website"
}
export interface IPlaceStateUpdate {
    placeId?: string;
    oldState: IPlaceState;
    newState: IPlaceState;
    updatedBy?: string;
    createdAt?: string;
    updatedAt?: string;
}
export interface IPlace {
    id?: string;
    location: ILocation;
    type: IPlaceType;
    state?: IPlaceState;
    createdAt?: string;
    updatedAt?: string;
    placeDetails?: IPlaceDetail[];
    profilePicture?: IPicture;
    pictures?: IPicture[];
    creatorId?: string;
    ownerId?: string;
}
export interface IPlaceVerification {
    id?: string;
    userId?: string;
    placeId?: string;
    createdAt?: string;
    updatedAt?: string;
}
export interface IPlaceDetail {
    id?: string;
    placeId?: string;
    type: IPlaceDetailType;
    body: string;
    createdAt?: string;
    updatedAt?: string;
}
