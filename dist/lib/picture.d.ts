export interface IPicture {
    url: string;
}
export interface IPictureQualityPoint {
    id?: string;
    userId?: string;
    isHighQuality: boolean;
    createdAt?: string;
    updatedAt?: string;
}
