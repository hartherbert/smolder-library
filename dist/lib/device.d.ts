export interface IDevice {
    id?: string;
    userId?: string;
    manufacturer?: string;
    platform?: string;
    uuid: string;
    model?: string;
    serial?: string;
    version?: string;
    deviceId?: string;
    createdAt?: string;
    updatedAt?: string;
}
