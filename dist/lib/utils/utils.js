"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function toCamelCase(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function (match, index) {
        if (+match === 0)
            return ''; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
    });
}
exports.toCamelCase = toCamelCase;
//# sourceMappingURL=utils.js.map