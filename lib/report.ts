export enum IReportType {
  NotAvailableAnymore = 'not available anymore',
  Malfunction = 'malfunction',
  TemporarilyClosed = 'temporarily closed',

  Custom = 'custom',
}

export interface IReport {
  id?: string;
  type: IReportType;
  placeId?: string;
  userId?: string;
  body?: string; // report message
  checked?: boolean; // if report has been checked by admin
  active?: boolean; // if report is still relevant
  createdAt?: string;
  updatedAt?: string;
}
