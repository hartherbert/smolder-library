import { IPicture } from './picture';
import { ILocation } from './location';

export enum IPlaceType {
  ATM = 'atm',
  Tobacco = 'tobacco',
  Cigarettes = 'cigarettes',
  Fuel = 'fuel',
  Bar = 'bar',
  Pub = 'pub',
  Bank = 'bank',
  Pharmacy = 'pharmacy',
  Casino = 'casino',
  NightClub = 'nightclub',
  Toilets = 'toilets',
  Hospital = 'hospital',
  Alcohol = 'alcohol',
  Kiosk = 'kiosk',
}

export enum IPlaceState {
  New = 'new',
  Verified = 'verified',
  Perchance = 'perchance',
  Outdated = 'outdated',
}

export enum IPlaceDetailType {
  Name = 'name',
  OpeningHours = 'opening',
  IsInside = 'inside',
  Brand = 'brand',
  Email = 'email',
  Website = 'website',
}

export interface IPlaceStateUpdate {
  placeId?: string;
  oldState: IPlaceState;
  newState: IPlaceState;
  updatedBy?: string;
  createdAt?: string;
  updatedAt?: string;
}

export interface IPlace {
  id?: string;
  location: ILocation;
  type: IPlaceType;
  state?: IPlaceState;
  createdAt?: string;
  updatedAt?: string;
  placeDetails?: IPlaceDetail[];

  // profile picture of place
  profilePicture?: IPicture;

  pictures?: IPicture[];

  creatorId?: string;
  ownerId?: string;
}

export interface IPlaceVerification {
  id?: string;

  userId?: string;
  placeId?: string;

  createdAt?: string;
  updatedAt?: string;
}

export interface IPlaceDetail {
  id?: string;
  placeId?: string;
  type: IPlaceDetailType;
  body: string;
  createdAt?: string;
  updatedAt?: string;
}
