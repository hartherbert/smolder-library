"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var LoginMethod;
(function (LoginMethod) {
    LoginMethod["Google"] = "google";
    LoginMethod["Facebook"] = "facebook";
    LoginMethod["Twitter"] = "twitter";
    LoginMethod["Custom"] = "custom";
})(LoginMethod = exports.LoginMethod || (exports.LoginMethod = {}));
//# sourceMappingURL=auth.js.map