import { LoginMethod } from './auth';
import { IDevice } from './device';

export interface IAppUser {
  id?: string;
  socialUserId?: string;
  loginMethod?: LoginMethod;
  name?: string;
  email: string;


  isEmailVerified?: boolean;
  isActive?: boolean;
  pictureUrl?: string;
  createdAt?: string;
  updatedAt?: string;
}

export interface IAppUserProfile {
  id?: string;
  userId?:string; // user reference
  language?: string;
}


export interface IAppUserClient {
  id?: string;
  token: string; // refresh-token
  userId?: string;
  ip?: string;
  device?: IDevice;
  userAgent?: string;
  createdAt?: string;
  updatedAt?: string;
}
