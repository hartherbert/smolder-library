"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IPlaceType;
(function (IPlaceType) {
    IPlaceType["ATM"] = "atm";
    IPlaceType["Tobacco"] = "tobacco";
    IPlaceType["Cigarettes"] = "cigarettes";
    IPlaceType["Fuel"] = "fuel";
    IPlaceType["Bar"] = "bar";
    IPlaceType["Pub"] = "pub";
    IPlaceType["Bank"] = "bank";
    IPlaceType["Pharmacy"] = "pharmacy";
    IPlaceType["Casino"] = "casino";
    IPlaceType["NightClub"] = "nightclub";
    IPlaceType["Toilets"] = "toilets";
    IPlaceType["Hospital"] = "hospital";
    IPlaceType["Alcohol"] = "alcohol";
    IPlaceType["Kiosk"] = "kiosk";
})(IPlaceType = exports.IPlaceType || (exports.IPlaceType = {}));
var IPlaceState;
(function (IPlaceState) {
    IPlaceState["New"] = "new";
    IPlaceState["Verified"] = "verified";
    IPlaceState["Perchance"] = "perchance";
    IPlaceState["Outdated"] = "outdated";
})(IPlaceState = exports.IPlaceState || (exports.IPlaceState = {}));
var IPlaceDetailType;
(function (IPlaceDetailType) {
    IPlaceDetailType["Name"] = "name";
    IPlaceDetailType["OpeningHours"] = "opening";
    IPlaceDetailType["IsInside"] = "inside";
    IPlaceDetailType["Brand"] = "brand";
    IPlaceDetailType["Email"] = "email";
    IPlaceDetailType["Website"] = "website";
})(IPlaceDetailType = exports.IPlaceDetailType || (exports.IPlaceDetailType = {}));
//# sourceMappingURL=place.js.map